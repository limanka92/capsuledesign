export class Option {
    private _label: string;
    private _value: string;

    constructor(label: string) {
        this._label = label;
        this._value = label.toUpperCase().replace(" ", "_");
    }

    public get label() {
        return this._label;
    }

    public get value() {
        return this._value;
    }

    public toString() {
        return this._label;
    }
}
