import { SelectableOption } from "./SelectableOption";

export class DoorOption extends SelectableOption {
    private _img: string;

    constructor(label: string, description: string, selectables: string[], img: string) {
        super(label, description, selectables);

        this._img = img;
    }

    public get img() {
        return this._img;
    }
}
