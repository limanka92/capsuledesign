import Radio from "@material-ui/core/Radio";
import Typography from "@material-ui/core/Typography";
import classNames from "classnames";
import React, { Component } from "react";

import { Capsule, CapsuleType, Option } from "client/models";

// tslint:disable: max-line-length

import { RadioButtonIcon, withStyles, WithStyles } from "client/ui";

const styles = () => ({
    bold: {
        fontWeight: "bold",
    },
    content: {
        textTransform: "uppercase",
    },
    dot: {
        ["&:before"]: {
            backgroundColor: "red",
            borderRadius: 5,
            content: "' '",
            display: "inline-block",
            height: 5,
            marginRight: 5,
            width: 5,
        },
        alignItems: "center",
        display: "flex",
    },
    flex: {
        display: "flex",
    },
    inline: {
        display: "inline-block",
    },
    label: {
        marginBottom: 20,
    },
    large: {
        fontSize: 28,
    },
    radio: RadioButtonIcon.RadioButtonIconStyle,
    radioClass: {
        marginRight: 14,
    },
    red: {
        color: "#e64a4a",
    },
    regularText: {
        textTransform: "none",
    },
    row: {
        display: "flex",
        flexDirection: "row",
    },
});
interface ISummaryProps extends WithStyles<typeof styles> {
    capsule: Capsule;
    options: Option[];
    selected: string[];
    onSelect: (val: string) => void;
}
@withStyles(styles)
export class SummaryComponent extends Component<ISummaryProps> {
    public render() {
        const { classes, options, selected } = this.props;

        return (
            <div>
                <Typography variant="title">Итого</Typography>
                <div className={classes.row}>
                    <div className={classes.content}>
                        <Typography className={classes.large}>80 000 руб.</Typography>
                        <div className={classNames(classes.row, classes.label)}>
                            <Radio
                                className={classes.radioClass}
                                classes={{ root: classes.radio }}
                                value={options[0].value}
                                checked={selected.indexOf(options[0].value) > -1}
                                onChange={this.onOptionSelect}
                                icon={<RadioButtonIcon checked={false} />}
                                checkedIcon={<RadioButtonIcon checked={true} />}
                            />
                            <Typography>Стоимость дизайн-проекта Без ремонта</Typography>
                        </div>
                        <div>
                            <Typography className={classes.bold}>ДИЗАЙН-ПРОЕКТ:</Typography>
                            <Typography>Срок разработки проекта - 10 раб дней.</Typography>
                            <br/>
                            <Typography className={classNames(classes.bold, classes.dot)}>ВЫБОР СТИЛЕВОГО НАСТРОЕНИЯ ПО ВИЗУАЛИЗАЦИИ (КАПСУЛА);</Typography>
                            <Typography className={classNames(classes.bold, classes.dot)}>РАЗРАБОТКА ПАКЕТА РАБОЧИХ ЧЕРТЕЖЕЙ ДЛЯ РЕМОНТА;</Typography>
                            <br/>
                            <Typography className={classes.bold}>СОСТАВ ПАКЕТА:</Typography>
                            <br/>
                            <Typography>1. Выезд на обмер</Typography>
                            <Typography>2. План обмера с привязкой инженерных коммуникаций</Typography>
                            <Typography>3. Кладочный план конструкций и перегородок</Typography>
                            <Typography>4. План расстановки мебели с габаритными размерами</Typography>
                            <Typography>5. План размещения светильников</Typography>
                            <Typography>6. План размещения выключателей</Typography>
                            <Typography>7. План привязки электроприводов, розеток</Typography>
                            <Typography>8. План размещения теплого пола и приборов отопления</Typography>
                            <Typography>9. План потолков с указанием конструкции, типа материала, размеров и привязок</Typography>
                            <Typography>10. План напольных покрытий с указанием типов используемых материалов</Typography>
                            <Typography>11. План размещения сантехники</Typography>
                            <Typography>12. План отделки стен</Typography>
                            <br/>
                            <Typography className={classNames(classes.bold, classes.dot)}>СОГЛАСОВАНИЕ БЮДЖЕТА;</Typography>
                            <br/>
                            <Typography className={classes.bold}>
                                КОМПЛЕКТАЦИЯ КАПСУЛЫ:
                            </Typography>
                            <br/>

                            <Typography className={classes.bold}>
                                Срок разработки - 15 раб дней.
                            </Typography>
                            <br/>
                            <Typography>
                                Комплектация — это процесс подбора, покупки, доставки материалов, мебели и оборудования для реализации дизайн-проекта. Осуществляется на этапе ремонта.
                            </Typography>
                            <br/>
                            <Typography>1. Ведомость чистовых материалов с артикулами. </Typography>
                            <Typography>2. Ведомость мебели и оборудования (кухня, зоны хранения, сантехника, мягкая мебель, свет, двери, декор, текстиль) </Typography>
                            <Typography>3. Рекомендации по приобретению со скидками (при удаленной работе)</Typography>
                        </div>
                    </div>
                    <div className={classes.content}>
                        <Typography className={classes.large}>{this.getPrice()}</Typography>
                        <div className={classNames(classes.row, classes.label)}>
                            <Radio
                                className={classes.radioClass}
                                classes={{ root: classes.radio }}
                                value={options[1].value}
                                checked={selected.indexOf(options[1].value) > -1}
                                onChange={this.onOptionSelect}
                                icon={<RadioButtonIcon checked={false} />}
                                checkedIcon={<RadioButtonIcon checked={true} />}
                            />
                            <Typography>реализация капсулы - все работы под ключ (Москва, Подмосковье и САНКТ-ПЕТЕРБУРГ).</Typography>
                        </div>
                        <div>
                        <Typography className={classes.regularText}>*Фактическая сумма может немного отличаться.</Typography>
                        <Typography className={classes.regularText}>Запрашивайте точный расчет у вашего менеджера.</Typography>
                        <br/>
                        <Typography className={classes.bold}>РЕМОНТ:</Typography>
                        <Typography className={classNames(classes.bold, classes.dot)}>СОГЛАСОВАНИЕ СМЕТНОГО РАСЧЕТА;</Typography>
                        <br/>
                        <div className={classes.flex}>
                            <Typography className={classNames(classes.bold, classes.dot)}>ЧЕРНОВОЙ ЭТАП </Typography>
                            <Typography className={classes.inline}>(материалы учтены в стоимость):</Typography>
                        </div>
                        <Typography>1. Возведение перегородок</Typography>
                        <Typography>2. Монтаж электроснабжения и слаботочных сетей</Typography>
                        <Typography>3. Монтаж отопления</Typography>
                        <Typography>4. Монтаж водоснабжения</Typography>
                        <Typography>5. Штукатурные работы</Typography>
                        <Typography>6. Работы по стяжке</Typography>
                        <br/>
                        <div className={classes.flex}>
                            <Typography className={classNames(classes.bold, classes.dot)}>ЧИСТОВОЙ ЭТАП </Typography>
                            <Typography className={classes.inline}>(материалы учтены в стоимость):</Typography>
                        </div>

                        <Typography>1. Малярные работы</Typography>
                        <Typography>2. Монтаж откосов</Typography>
                        <Typography>3. Гипсокартонные работы, а также монтаж инженерных коробов</Typography>
                        <Typography>4. Подготовительные работы для пола и потолка</Typography>
                        <Typography>5. Пуско-наладочные работы</Typography>
                        <Typography>6. Работа с чистовыми материалами</Typography>
                        <br/>
                        <Typography className={classes.bold}>ЭТОТ ПАКЕТ ПОДХОДИТ ЕСЛИ ВЫ:</Typography>
                        <Typography>1. Планируете инвестировать в арендную квартиру;</Typography>
                        <Typography>2. Хотите в кротчайшие сроки заехать в собственную квартиру;</Typography>
                        <Typography>3. Купили квартиру, но не хотите больших финансовых вливаний;</Typography>
                        <Typography>4. Улетаете на зимовку в теплые страны, и хотите вернуться уже в обновленную квартиру;</Typography>
                        <Typography>5. Мечтаете, чтобы было доступно, быстро и результативно</Typography>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    private getPrice = () => {
        switch (this.props.capsule.type) {
            case CapsuleType.marshmallow:
                return "2 000 000 руб. из расчета на 50 кв.м.";
            case CapsuleType.amnezia:
            case CapsuleType.anonimous:
                return "1 500 000 руб. из расчета на 50 кв.м.";
            case CapsuleType.transformer:
                return "ОТ 750 000 ДО 999 000 руб. (ОТ 18 ДО 40 КВ. М.)";
            default:
                return "";
        }
    }

    private onOptionSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.onSelect(e.target.value);
    }
}
