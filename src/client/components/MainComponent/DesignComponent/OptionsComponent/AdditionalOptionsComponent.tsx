import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import classNames from "classnames";
import React, { Component } from "react";

import { Option } from "client/models";
import { RadioButtonIcon, withStyles, WithStyles } from "client/ui";

const styles = () => ({
    column: {
        display: "flex",
        flexDirection: "column",
    },
    option: {
        ["&:last-child"]: {
            marginBottom: 0,
        },
        display: "flex",
        marginBottom: 24,
        textTransform: "uppercase",
    },
    radio: RadioButtonIcon.RadioButtonIconStyle,
    radioClass: {
        marginRight: 14,
    },
    row: {
        display: "flex",
        flexDirection: "row",
    },
    wrapper: {
        marginRight: 24,
        width: "20%",
    },
});

interface IAdditionalOptionsProps extends WithStyles<typeof styles> {
    options: Option[];
    selected: string[];
    onSelect: (val: string) => void;
}

//  tslint:disable-next-line array-type
function chunk<T>(arr: T[], chunkSize: number): Array<Array<T>> {
    return arr.reduce((prevVal: any, currVal: any, currIndx: number, array: T[]) =>
        !(currIndx % chunkSize) ?
        prevVal.concat([array.slice(currIndx, currIndx + chunkSize)]) :
        prevVal, []);
}
@withStyles(styles)
export class AdditionalOptionsComponent extends Component<IAdditionalOptionsProps> {
    public render() {
        const { classes, options } = this.props;
        const opts = chunk<Option>(options, 3);
        return (
            <div>
                <Typography variant="title">Дополнительные опции (оплачиваются отдельно)</Typography>
                <div className={classes.row}>
                    {opts.map(this.renderWrapper)}
                </div>
            </div>
        );
    }

    private renderWrapper = (options: Option[], index: number) => {
        const { classes } = this.props;
        return (
            <div key={index} className={classNames(classes.column, classes.wrapper)}>
                {options.map(this.renderOption)}
            </div>
        );
    }

    private renderOption = (option: Option) => {
        const { classes, selected } = this.props;

        return (
            <div
                key={option.value}
                className={classes.option}
            >
                <Checkbox
                    value={option.value}
                    className={classes.radioClass}
                    checked={selected.indexOf(option.value) >= 0}
                    classes={{ root: classes.radio }}
                    onChange={this.onOptionSelect}
                    icon={<RadioButtonIcon checked={false} />}
                    checkedIcon={<RadioButtonIcon checked={true} />}
                />
                <Typography>{option.label}</Typography>
            </div>
        );
    }

    private onOptionSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.onSelect(e.target.value);
    }
}
