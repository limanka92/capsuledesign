import Checkbox from "@material-ui/core/Checkbox";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import Typography from "@material-ui/core/Typography";
import classNames from "classnames";
import React, { Component } from "react";

import { observer } from "mobx-react";

import { SelectableOption } from "client/models";
import { RadioButtonIcon, withStyles, WithStyles } from "client/ui";

const styles = () => ({
    caption: {
        color: "#bbbbbb",
        fontSize: 15,
    },
    checked: {
        border: "1px solid",
    },
    label: {
        color: "#161616",
        marginBottom: 20,
        textTransform: "uppercase",
    },
    option: {
        alignItems: "flex-start",
        width: "50%",
    },
    radio: RadioButtonIcon.RadioButtonIconStyle,
    radioClass: {
        marginRight: 14,
    },
    row: {
        display: "flex",
        flexDirection: "row",
    },
    variant: {
        borderRadius: "50%",
        height: 40,
        width: 40,
    },
    variantRoot: {
        ["&:last-child"]: {
            marginRight: 0,
        },
        marginRight: 24,
        padding: 5,
    },
    variants: {
        flexDirection: "row",
        marginTop: 14,
    },
});

interface IWallOptionProps extends WithStyles<typeof styles> {
    options: SelectableOption[];
    selected: string[];
    onSelect: (val: string) => void;
}

@withStyles(styles)
@observer
export class WallOptionComponent extends Component<IWallOptionProps> {
    public render() {
        const { classes, options } = this.props;

        return (
            <div>
                <Typography variant="title">Стены</Typography>
                <div className={classes.row}>
                    {options.map(this.renderOption)}
                </div>
            </div>
        );
    }

    private renderOption = (option: SelectableOption) => {
        const { classes, selected } = this.props;
        return (
            <div key={option.value} className={classNames(classes.row, classes.option)}>
                <Checkbox
                    value={option.value}
                    className={classes.radioClass}
                    checked={selected.indexOf(option.value) >= 0}
                    classes={{ root: classes.radio }}
                    onChange={this.onOptionSelect}
                    icon={<RadioButtonIcon checked={false} />}
                    checkedIcon={<RadioButtonIcon checked={true} />}
                />
                <div>
                    <Typography className={classes.label}>{option.label}</Typography>
                    <Typography className={classes.caption} variant="caption">
                        {option.description}
                    </Typography>
                    <RadioGroup
                        value={option.selected}
                        className={classes.variants}
                        onChange={this.onVariantSelect.bind(option)}
                    >
                        {option.selectables.map(this.renderVariant)}
                    </RadioGroup>
                </div>
            </div>
        );
    }

    private onOptionSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.onSelect(e.target.value);
    }

    private onVariantSelect(e: React.ChangeEvent<HTMLInputElement>) {
        // @ts-ignore
        this.setSelected(e.target.value);
    }

    private renderVariant = (variant: string) => {
        const { classes } = this.props;
        const radioClasses = {
            checked: classes.checked,
            root: classes.variantRoot,
        };
        return (
            <Radio
                classes={radioClasses}
                key={variant}
                value={variant}
                icon={this.renderIcon(variant)}
                checkedIcon={this.renderIcon(variant)}
            />
        );
    }

    private renderIcon = (variant: string) => {
        const { classes } = this.props;

        return (
            <div
                className={classes.variant}
                style={{ backgroundColor: variant }}
            />
        );
    }
}
