import classNames from "classnames";
import React, { Component } from "react";

import { withStyles, WithStyles } from "./withStyles";

const styles = () => ({
    contentWrapper: {
        margin: "0 auto",
        maxWidth: 1600,
    },
});

interface IContentWrapperProps extends WithStyles<typeof styles> {
    className?: string;
}

@withStyles(styles)
export class ContentWrapperComponent extends Component<IContentWrapperProps> {
    public render() {
        const { className, classes, children } = this.props;
        return (
            <div className={classNames(classes.contentWrapper, className)}>
                {children}
            </div>
        );
    }
}
