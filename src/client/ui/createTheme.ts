import { createMuiTheme } from "@material-ui/core";

const inputText = {
    fontFamily: "'Open Sans'",
    fontSize: 15,
    fontWeight: 400,
};

const pointChecked =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/point-active.svg";

export const createTheme = () => createMuiTheme({
    overrides: {
        MuiFormLabel: {
            root: {
                ["&$focused"]: {
                    color: "inherit",
                },
                color: "#0a0a0a",
                fontFamily: "'Open Sans'",
                fontSize: 15,
                fontWeight: 400,
                textTransform: "uppercase",
            },
        },
        MuiIconButton: {
            label: {
                ["&:hover > .capsule-radio"]: {
                    backgroundImage: `url(${pointChecked}) !important`,
                },
                height: "100%",
            },
        },
        MuiInput: {
            input: {
                ["&::placeholder"]: {
                    color: "#green",
                },
                ["&::-webkit-input-placeholder"]: {
                    color: "#green",
                },
                ["&::-moz-placeholder"]: {
                    color: "#green",
                },
                ["&:-ms-input-placeholder"]: {
                    color: "#green",
                },
                ["&:-moz-placeholder"]: {
                    color: "#green",
                },
                fontFamily: "'Open Sans'",
                fontSize: 15,
                fontWeight: 400,
            },
            root: {
                paddingTop: 10,
            },
            underline: {
                ["&::before"]: {
                    borderBottomColor: "#0a0a0a",
                },
                ["&::after"]: {
                    borderBottomColor: "#0a0a0a",
                },
            },
        },
        MuiInputLabel: {
            shrink: {
                transform: "translate(0, 1.5px)",
            },
        },
    },
    typography: {
        fontFamily: "Open Sans",
        title: {
            color: "#e64a4a",
            fontFamily: "Arial",
            fontSize: "38.76px",
            fontWeight: 700,
            marginBottom: 20,
            textAlign: "left",
        },
    },
});
